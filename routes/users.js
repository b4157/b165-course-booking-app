// [SECTION] Dependencies and Modules
	const exp = require("express");
	const controller = require('../controllers/users');
	const auth = require("../auth");

	// destructure verify from auth
  	const {verify, verifyAdmin} = auth;


// [SECTION] Routing Component
	const route = exp.Router();

// [SECTION] Routes - [POST]

	route.post('/register', (req, res) => {
		let userDetails = req.body;
	
		controller.registerUser(userDetails).then(outcome => {
			res.send(outcome);
		});
	});

	// route.post('/login', (req, res) => {
	// 	let userDetails = req.body;
	// 	controller.loginUser(userDetails).then(outcome=>{
	// 		res.send(outcome);
	// 	});
	// });

	// LOGIN
  
  	route.post("/login", controller.loginUser);

// GET USER DETAILS
  
  // note: routes that have verify as a middleware would require us to pass a token from postman
  route.get("/getUserDetails", verify, controller.getUserDetails)


  //implement a route for the user authentication
    //why POST? => because this request setup will include a BODY SECTION, so we cannot simply use GET, GET ===> no body section
    //static?
  route.post('/login', (req, res) => {
     //console.log(req.body);
     let userDetails = req.body; 
     //display the input from the user in cli.
     controller.loginUser(userDetails).then(outcome => {
        //send the outcome back to the client
        res.send(outcome);
     });
  }); 
// [SECTION] Routes - [GET]
	route.get('/getEnrollments', verify, controller.getEnrollments)

// GET USER DETAILS
  
  // note: routes that have verify as a middleware would require us to pass a token from postman
  route.get("/getUserDetails", verify, controller.getUserDetails)

	route.get('/all', (req, res) => {

	    controller.getAllUsers().then(result => {
	    	res.send(result);
	    });
	});

	route.post('/enroll', verify, controller.enroll)

// [SECTION] Routes - [PUT]
// [SECTION] Routes - [DELETE]

// [SECTION] Routes Expose Route System
	module.exports = route;









