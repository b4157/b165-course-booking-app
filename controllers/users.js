// [SECTION] Dependencies and modules
	const auth = require("../auth")
	const User = require('../models/user')
	const Course = require('../models/course');
	const bcrypt = require("bcrypt");  
	const dotenv = require("dotenv"); 
	

// [SECTION] Environment Setup
	dotenv.config();
	const salt = parseInt(process.env.SALT);

// [SECTION] Functionalities [CREATE]

	module.exports.registerUser = (data) => {
		
		let fName = data.firstName;
		let lName = data.lastName;
		let email = data.email;
		let passW = data.password;
		let gendr = data.gender;
		let mobil = data.mobileNo;

		let newUser = new User({
			
				firstName: fName,
				lastName: lName,
				email: email,
				password: bcrypt.hashSync(passW,salt),
				gender: gendr,
				mobileNo: mobil
		});

		return newUser.save().then( (userCreated, rejected) => {
			if (userCreated) {
				return userCreated;
			} else {
				return 'Failed to Register new account'
			}	
		});
	};

// <<<<<<< HEAD
   module.exports.loginUser = (userData) => {
    let email = userData.email;
    let passW = userData.password;
    
    return User.findOne({email: email}).then(result => {
       if (result === null) {
          return 'Email Not Found'; 
       } else {
          return 'Email Found'; 
       };
    });
  };

// LOGIN 

	module.exports.loginUser = (req, res) => {
	    console.log(req.body)

    /*
      Steps:
      1. Find the user by the email
      2. If we found a user, we will check the password
      3. If we don't find a user, we will send a message to the client
      4. If upon checking the found user's password is the same as the input's password, we will generate a "key" to access our app. If not, we will turn them away by sending a message to the client
    */
    	User.findOne({email: req.body.email}).then(foundUser => {

	      if(foundUser === null){
	      	
	        return res.send({message:"User Not Found"});

	      } else {

       		const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

        	console.log(isPasswordCorrect)
        
	      if(isPasswordCorrect){

	          return res.send({accessToken: auth.createAccessToken(foundUser)})

	       } else {

	          return res.send(false)
	       }

      }

    })
    .catch(err => res.send(err))

};

// GET USER DETAILS

	module.exports.getUserDetails = (req, res) => {

	    console.log(req.user);

	    User.findById(req.user.id).then(result => res.send(result))
	    .catch(err => res.send(err))
	};
	


// [SECTION] Functionalities [RETRIEVE]

	module.exports.getAllUsers = () => {
        return User.find( {} ).then(result => {
        return result;
     });
   };

   module.exports.getEnrollments = (req, res) => {
   		User.findById(req.user.id).then(result => res.send (result.enrollments))
   }
   


// [SECTION] Functionalities [UPDATE]

	module.exports.enroll = async (req,res) =>{
	 

	    if(req.user.isAdmin) {
	        return res.send("Action Forbidden")
	    }

	    // enrol course per student
	    let isUserUpdated = await User.findById(req.user.id).then(user => {

	        let newEnrollment = {
	            courseId: req.body.courseId
	        }

	        user.enrollments.push(newEnrollment);
	        return user.save().then(user => true).catch(err => err.message)

		    })

	    if(isUserUpdated !== true){
	        return res.send({message: isUserUpdated})
	    }


	    // saving enrolled students per course
	    let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {

	        let newEnrollee = {
	            userId: req.user.id
	        }

	        course.enrollees.push(newEnrollee);
	        return course.save().then(course => true).catch(err => err.message)

	    })

	    if(isCourseUpdated !== true){
	        return res.send({message: isCourseUpdated})
	    }

	    if(isCourseUpdated == true && isUserUpdated == true){
	    	return res.send("Enrolment successful!")
	    }

	}
// [SECTION] Functionalities [DELETE]

// let tAmount = req.body.tAmount
// let addProduct = [{
// 	"productId": pId,
// 	"quantity": qty
// }]

// let newOrder = new Order({
// 	totalAmount: tAmount, 
// 	userId: uId,
// 	orders: addProduct
// })










